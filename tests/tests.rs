// See http://gameprogrammingpatterns.com/bytecode.html

extern crate carabined;

use std::cell::RefCell;
use std::rc::Rc;
use std::boxed::Box;

use std::fmt::{Display, Formatter, self};
use std::io::{stdout, Write};

use carabined::Callable;
use carabined::Vm;
use carabined::State;

struct Personnage {
    health: u8,
    wisdom: u8,
    agility: u8,
}

struct Literal;
struct Add;
struct Div;

struct SetHealth {
    perso: Rc<RefCell<Personnage>>,
}

struct GetHealth {
    perso: Rc<RefCell<Personnage>>,
}

struct GetAgility {
    perso: Rc<RefCell<Personnage>>,
}

struct GetWisdom {
    perso: Rc<RefCell<Personnage>>,
}

impl Display for Literal {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "LITERAL")
    }
}

impl Callable for Literal {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        let x = match mem.code.next() {
            Some(x) => x,
            None => 0,
        };
        mem.stack.push(x);

        None
    }

    fn len(&self) -> usize {
        1
    }
}

impl Display for Add {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "ADD")
    }
}

impl Callable for Add {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        let x = match mem.stack.pop::<u8>() {
            Some(x) => x,
            None => 0,
        };
        let y = match mem.stack.pop::<u8>() {
            Some(y) => y,
            None => 0,
        };
        mem.stack.push(x + y);

        None
    }

    fn len(&self) -> usize {
        0
    }
}

impl Display for Div {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "ADD")
    }
}

impl Callable for Div {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        let y = match mem.stack.pop::<u8>() {
            Some(y) => y,
            None => 0,
        };
        let x = match mem.stack.pop::<u8>() {
            Some(x) => x,
            None => 0,
        };
        mem.stack.push(x / y);

        None
    }

    fn len(&self) -> usize {
        0
    }
}

impl Display for SetHealth {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "SetHealth")
    }
}

impl Callable for SetHealth {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        let x = match mem.stack.pop::<u8>() {
            Some(x) => x,
            None => 0,
        };
        let _ = mem.stack.pop::<u8>();
        self.perso.borrow_mut().health = x;
        None
    }

    fn len(&self) -> usize {
        0
    }
}

impl Display for GetHealth {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "GetHealth")
    }
}

impl Callable for GetHealth {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        let _ = mem.stack.pop::<u8>();
        mem.stack.push(self.perso.borrow().health);
        None
    }

    fn len(&self) -> usize {
        0
    }
}

impl Display for GetWisdom {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "GetWisdom")
    }
}

impl Callable for GetWisdom {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        let _ = mem.stack.pop::<u8>();
        mem.stack.push(self.perso.borrow().wisdom);
        None
    }

    fn len(&self) -> usize {
        0
    }
}

impl Display for GetAgility {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "GetAgility")
    }
}

impl Callable for GetAgility {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        let _ = mem.stack.pop::<u8>();
        mem.stack.push(self.perso.borrow().agility);
        None
    }

    fn len(&self) -> usize {
        0
    }
}

#[test]
fn it_works() {
    let pp = Personnage {
        health:100,
        wisdom:10,
        agility: 10,
    };
    let rc = Rc::new(RefCell::new(pp));
    let mut inst_array: Vec<Box<dyn Callable>> = vec![
        Box::new(Literal),                      // 0
        Box::new(Add),                          // 1
        Box::new(Div),                          // 2
        Box::new(GetHealth{perso:rc.clone()}),  // 3
        Box::new(GetWisdom{perso:rc.clone()}),  // 4
        Box::new(GetAgility{perso:rc.clone()}), // 5
        Box::new(SetHealth{perso:rc.clone()}),  // 6
    ];
    let bc = vec![
        //Instruction, value
        0 as u8, 0 as u8,
        0 as u8, 0,
        3 as u8,
        0 as u8, 0,
        5 as u8,
        0 as u8, 0,
        4 as u8,
        1 as u8,
        0 as u8, 2,
        2 as u8,
        1 as u8,
        6 as u8,
    ];
    let mut test_vm = Vm::construct()
        .code(&bc)
        .heap(128)
        .stack(128)
        .build(&mut inst_array);

    test_vm.interpret();
    assert_eq!(rc.borrow().health, 110);
}
