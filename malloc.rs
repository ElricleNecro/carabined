extern crate carabined;

use std::boxed::Box;

use carabined::Callable;
use carabined::Vm;
use carabined::State;

struct Malloc;
struct Get;
struct Affectation;
struct Add;

struct App {
    vm: Vm,
}

impl Callable for Malloc {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        let x = match mem.stack.pop::<u8>() {
            Some(x) => x,
            None => 0,
        };
        None
    }
}

impl Callable for Get {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        let x = match mem.stack.pop::<u8>() {
            Some(x) => x,
            None => 0,
        };

        mem.stack.push(
            mem.reg.pop::<u8>(x as usize)
        );

        None
    }
}

impl Callable for Affectation {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        None
    }
}

impl Callable for Add {
    fn call(&mut self, mem: &mut State) -> Option<usize> {
        let x = match mem.stack.pop::<u8>() {
            Some(x) => x,
            None => 0,
        };
        let y = match mem.stack.pop::<u8>() {
            Some(y) => y,
            None => 0,
        };
        mem.stack.push(x + y);

        None
    }
}

fn init() -> App {
    let inst = vec![
        Box::new(Malloc),       // 0
        Box::new(Get),          // 1
        Box::new(Affectation),  // 2
        Box::new(Add),          // 3
        Box::new(Print),        // 4
    ];

    let bc = vec![
        3 as u8,
    ];

     App {
        vm: Vm::new(
                bc,
        ),
    }
}

pub fn main() {
    let app = init();
}
