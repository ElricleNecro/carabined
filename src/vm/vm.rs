use std::boxed::Box;
use std::fmt;

use memory::Stack;
use memory::Heap;
use memory::Register;
use memory::Code;

pub struct State<'a> {
    pub stack: Stack,
    pub heap: Heap,
    pub reg: Register,
    pub code: Code<'a>,
    pub fp: usize,
}

pub struct Vm<'a> {
    pub mem: State<'a>,
    pub inst: &'a mut Vec<Box<dyn Callable>>,

    debug: bool,
}

macro_rules! set_basic_instruction {
    ($x:tt) => (
            struct Inst_$x {}
            impl Callable for Inst_$x {
                pub fn call(&mut self, s:&mut State) -> Option<usize> {
                    $x(s)
                }
            }
    )
}

pub trait Callable : fmt::Display {
    fn call(&mut self, s:&mut State) -> Option<usize>;
    fn len(&self) -> usize;
}

impl<'a> Vm<'a> {
    pub fn new(instructions: &'a mut Vec<Box<dyn Callable>>, bytescode: &'a Vec<u8>) -> Vm<'a> {
        Vm {
            mem: State {
                stack: Stack::new(256),
                heap: Heap::new(256),
                reg: Register::new(),
                code: Code::new(bytescode),
                fp: 0,
            },
            inst: instructions,
            debug: false,
        }
    }

    pub fn new_with_capacity(instructions: &'a mut Vec<Box<dyn Callable>>, bytescode: &'a Vec<u8>, capacity: usize) -> Vm<'a> {
        Vm {
            mem: State {
                stack: Stack::new(capacity),
                heap: Heap::new(capacity),
                reg: Register::new(),
                code: Code::new(bytescode),
                fp: 0,
            },
            inst: instructions,
            debug: false,
        }
    }

    pub fn new_code(&mut self, code: &'a Vec<u8>) {
        self.mem.code = Code::new(code);
    }

    pub fn add_instruction(&mut self, to_add: &'a mut Vec<Box<Callable>>) {
        self.inst = to_add;
    }

    pub fn verbose(&mut self) {
        self.debug = true;
    }

    pub fn construct() -> VmBuilder<'a> {
        VmBuilder::start()
    }

    fn print_opcode(&self, opcode: u8) -> String {
        let it = &self.inst[opcode as usize];
        let mut printable = format!("{:04}: {:6} ", self.mem.code.eip, it);
        for idx in 0..it.len() {
            printable = format!("{} {:x} ", printable, self.mem.code.peek_next(idx));
        }

	printable
    }

    pub fn exec(&mut self, opcode: u8) {
        if self.debug {
            eprint!("{:<39}", self);
        }

        self.inst[opcode as usize].call(&mut self.mem);

        if self.debug {
            eprintln!(" {:>39}", self.mem.stack);
        }
    }

    pub fn next(&mut self) -> Option<u8> {
        self.mem.code.next()
    }

    pub fn interpret(&mut self) {
        while let Some(opcode) = self.mem.code.next() {
            self.exec(opcode);
        }
    }
}

pub struct VmBuilder<'a> {
    stack: usize,
    heap: usize,
    reg: usize,
    code: Option<&'a Vec<u8>>,

    debug: bool,
}

impl<'a> VmBuilder<'a> {
    pub fn start() -> Self {
        Self {
            stack: 256,
            heap: 256,
            reg: 0,
            code: None,

            debug: false,
        }
    }

    pub fn stack(&mut self, size: usize) -> &mut Self {
        self.stack = size;

        self
    }

    pub fn heap(&mut self, size: usize) -> &mut Self {
        self.heap = size;

        self
    }

    pub fn register(&mut self, size: usize) -> &mut Self {
        self.reg = size;

        self
    }

    pub fn code(&mut self, bc: &'a Vec<u8>) -> &mut Self {
        self.code = Some(bc);

        self
    }

    // pub fn instructions(&mut self, inst: &'a mut Vec<Box<dyn Callable>>) -> &mut Self {
        // self.inst = Some(inst);

        // self
    // }

    pub fn verbose(&mut self) -> &mut Self {
        self.debug = true;

        self
    }

    pub fn build(&self, inst: &'a mut Vec<Box<dyn Callable>>) -> Vm<'a> {
        Vm {
            mem: State {
                stack: Stack::new(self.stack),
                heap: Heap::new(self.heap),
                reg: Register::with_size(self.reg),
                code: Code::new(self.code.unwrap()),

                fp: 0,
            },

            inst: inst,
            debug: self.debug,
        }
    }
}

impl<'a> fmt::Display for Vm<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let opcode = self.mem.code.peek_at(self.mem.code.eip - 1);
        // let it = &self.inst[opcode as usize];
        let printable = self.print_opcode(opcode);
        /*let mut printable = format!("{:04}: {:6} ", self.mem.code.eip, it);

        for idx in 0..it.len() {
            printable = format!("{} {:x} ", printable, self.mem.code.peek_next(idx));
        }*/

        // printable = format!("{:<39} {:>39}", printable, self.mem.stack);

        f.pad_integral(true, "", &printable)
    }
}
