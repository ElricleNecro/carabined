// See http://gameprogrammingpatterns.com/bytecode.html
extern crate bincode;
#[macro_use]
extern crate serde_derive;
extern crate serde;

pub use self::memory::{Stack,Code,Register};
pub use self::vm::{Vm,Callable,State};

mod memory;
pub mod vm;

