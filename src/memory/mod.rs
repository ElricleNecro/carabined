pub use self::stack::Stack;
pub use self::code::Code;
pub use self::register::Register;
pub use self::heap::Heap;

pub mod code;
pub mod stack;
pub mod register;
pub mod heap;
