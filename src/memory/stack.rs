use serde;

use bincode::{serialize, deserialize};

use std::fmt;
use std::mem::size_of;
use std::ops::{Index,IndexMut};

#[derive(Debug)]
pub struct Stack {
    stack: Vec<u8>,
    stack_size: usize,
}

impl Stack {
    #[inline]
    pub fn new(capacity: usize) -> Stack {
        Stack {
            stack: Vec::with_capacity(capacity),
            stack_size: capacity,
        }
    }

    pub fn push_single(&mut self, value: u8) {
        if self.stack.len() < self.stack_size {
            self.stack.push(value);
        }
    }

    pub fn push<T>(&mut self, value: T)
        where T: serde::Serialize,
    {
        if self.stack.len() < self.stack_size {
            self.stack.extend_from_slice(&serialize(&value).unwrap()[..]);
        }
    }

    pub fn pop_single(&mut self) -> Option<u8> {
        self.stack.pop()
    }

    pub fn pop<T>(&mut self) -> Option<T>
        where T: ?Sized + serde::de::DeserializeOwned,
    {
        let mut tmp: Vec<u8> = Vec::new();
        let elem: usize = size_of::<T>() / size_of::<u8>();
        for _ in 0..elem {
            tmp.push(self.stack.pop().unwrap());
        }
        tmp.reverse();

        Some(deserialize(&tmp[..]).unwrap())
    }

    pub fn sp(&self) -> usize {
        self.stack.len()
    }

    pub fn remove_to(&mut self, fp: usize) {
        self.stack.truncate(fp+1);
    }
}

impl Index<usize> for Stack {
    type Output = u8;

    fn index(&self, idx: usize) -> &u8 {
        &self.stack[idx]
    }
}

impl IndexMut<usize> for Stack {
    fn index_mut(&mut self, idx: usize) -> &mut u8 {
        &mut self.stack[idx]
    }
}

impl fmt::Display for Stack {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let print = format!("{:?}", self.stack);
        f.pad_integral(true, "", &print)
    }
}
