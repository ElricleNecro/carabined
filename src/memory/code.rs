use std::ops::Index;
use std::iter::Iterator;
use std::convert::{From,Into};
use std::slice::Iter;

pub struct Code<'a> {
    pub eip: usize,
    pub bytescode: &'a Vec<u8>,
}

impl<'a> Code<'a> {
    #[inline]
    pub fn new(bc: &Vec<u8>) -> Code {
        Code {
            eip: 0,
            bytescode: bc,
        }
    }

    pub fn len(&self) -> usize {
        self.bytescode.len()
    }

    pub fn pointer(&self) -> usize {
        self.eip
    }

    pub fn jmp(&mut self, to: usize) -> Option<usize> {
        self.eip = to;

        None
    }

    pub(crate) fn peek_at(&self, idx: usize) -> u8 {
        self.bytescode[idx]
    }

    pub(crate) fn peek_next(&self, idx: usize) -> u8 {
        self.bytescode[self.eip + idx]
    }

    pub fn iter(&self) -> Iter<u8> {
        self.bytescode.iter()
    }
}

impl<'a> From<&'a Vec<u8>> for Code<'a> {
    fn from(v: &'a Vec<u8>) -> Self {
        Code {
            eip: 0,
            bytescode: &v,
        }
    }
}

impl<'a> Into<&'a Vec<u8>> for Code<'a> {
    fn into(self) -> &'a Vec<u8> {
        &self.bytescode
    }
}

impl<'a> Index<usize> for Code<'a> {
    type Output = u8;

    fn index(&self, idx: usize) -> &u8 {
        &self.bytescode[idx]
    }
}

impl<'a> Iterator for Code<'a> {
    type Item = u8;

    fn next(&mut self) -> Option<u8> {
        if self.eip >= self.bytescode.len() {
            return None;
        }

        let res = Some(self.bytescode[self.eip]);
        self.eip += 1;
        res
    }
}
