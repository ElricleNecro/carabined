extern crate bincode;

use serde;

use self::bincode::{serialize, deserialize};

use std::mem::size_of;
use std::fmt;

pub struct Heap {
    heap: Vec<u8>,
    // heap_size: usize,
}

impl Heap {
    #[inline]
    pub fn new(capacity: usize) -> Heap {
        Heap {
            heap: vec![0; capacity],
            // heap_size: capacity,
        }
    }

    pub fn set<T>(&mut self, addr: usize, value: T)
        where T: serde::Serialize,
    {
        if addr + size_of::<T>() < self.heap.len() {
            // TODO: Correct this bug: we should not extend!
            // self.heap.extend_from_slice(&serialize(&value).unwrap()[..]);
            for (offset, v) in serialize(&value).unwrap().into_iter().enumerate() {
                self.heap[addr + offset] = v;
            }
        }
    }

    pub fn get<T>(&self, addr: usize) -> Option<T>
        where T: ?Sized + serde::de::DeserializeOwned,
    {
        let mut tmp: Vec<u8> = Vec::new();
        let elem: usize = size_of::<T>() / size_of::<u8>();
        for i in addr..(addr + elem) {
            tmp.push(self.heap[ i ]);
        }
        tmp.reverse();

        Some(deserialize(&tmp[..]).unwrap())
    }
}

impl fmt::Display for Heap {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.heap)
    }
}
