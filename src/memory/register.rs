extern crate bincode;

use serde;
use self::bincode::{serialize, deserialize};

use std::mem::size_of;
// use std::collections::LinkedList;

pub struct Register {
    mem: Vec<u8>,
}

impl Register {
    #[inline]
    pub fn new() -> Register {
        Register {
            mem: Vec::new(),
        }
    }

    #[inline]
    pub fn with_size(size: usize) -> Self {
        Self {
            mem: vec![0; size],
        }
    }

    #[inline]
    pub fn get<T>(&self, idx: usize) -> T
        // where T: ?Sized,
        where T: ?Sized + serde::de::DeserializeOwned,
    {
        deserialize(&self.mem[idx..(idx+size_of::<T>())]).unwrap()
    }

    #[inline]
    pub fn set<T>(&mut self, idx: usize, val: T)
        // where T: ?Sized,
        where T: serde::Serialize,
    {
        let tmp: Vec<u8> = serialize(&val).unwrap();
        let mut i: usize = idx;

        for v in tmp {
            self.mem[i] = v;
            i += 1;
        }
    }
}
