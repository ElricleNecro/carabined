# Carabined
[![pipeline status](https://gitlab.com/ElricleNecro/carabined/badges/master/pipeline.svg)](https://gitlab.com/ElricleNecro/carabined/commits/master)
[![coverage report](https://gitlab.com/ElricleNecro/carabined/badges/master/coverage.svg)](https://gitlab.com/ElricleNecro/carabined/commits/master)

This library provide almost everything a person need to build a little and basic scripting language.

## Memory structure:
It provide structure to deal with different type of memory:

* Stack: a LIFO organized memory.
* Register: a memory accessible using addresses (index in these case).
* Code: a register type memory used to store the byte code.

## Interpreter:
From the interpreter side, it provide a structure ```Vm``` which has an ```interpret``` method. This
method will go through the bytes code.

If your script support functions, the main function has to be the last
block of code, and the first instruction of the bytes code should be a jump instruction (you have provided)
to the entry point.

## Instructions set:
This is the most difficult/annoying part. At the moment, there is no default instruction provided. You
have to provide them all, including the jump instruction.

To create and use an instruction, you have to create a vector containing `Box` of `Callable`. A `Callable`
is a trait define in the `vm` submodule. The definition is the following:
```rust
trait Callable {
    fn call(&mut self, s:&mut State) -> Option<usize>;
}
```

For example, to construct a jump instruction, you would do:
```rust
use carabined::Callable;
use carabined::State;

struct Jmp;

impl Callable for jmp {
	fn call(&mut self, mem: &mut State) -> Option<usize> {
		let x = match mem.stack.pop::<usize>() {
			Some(x) => x,
			None => panic!("Missing argument!"),
		}

		mem.code.jmp(x)
	}
}
```
Then, to use it and give to the interpreter, it's simple, but not pretty:
```rust
use carabined::Vm;

let my_bytes_code: Vec<u8> = ...;
let instruction: Vec<Box<Callable>> = vec![
	Box::new(Jmp),
];

let mut vm = Vm::new(my_bytes_code);
vm.add_instruction(instruction);

vm.interpret();
```
And that's it. Be careful, at the moment, `Vm::add_instruction` actually replace the instruction arrays
of the virtual machine, instead of extending it. It may be temporary, or stay like this. The API is not
yet fully stabilised.

For more complex example, you can look at the tests file.
