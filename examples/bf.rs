extern crate carabined;

/// in1 and in2 designate registers.
/// | Add | in1,in2 | Add in1 to in2 and store the results to in1. |
/// | Sub | in1,in2 | Sub in1 to in2 and store the results to in1. |
/// | Div | in1,in2 | Div in1 to in2 and store the results to in1. |
/// | Mul | in1,in2 | Mul in1 to in2 and store the results to in1. |

use carabined::{Callable, State, Vm};

use std::env;
use std::fmt::{Display, Formatter, self};
use std::fs::File;
use std::io::{stdout, stdin, Write, Read};
use std::path::Path;

/// No operation instruction.
///
/// Does nothing.
struct Nop;
impl Display for Nop {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "NOP")
    }
}

impl Callable for Nop {
    fn call(&mut self, _s: &mut State) -> Option<usize> {
        None
    }

    fn len(&self) -> usize {
        0
    }
}

/// Add two register and store the result in the first one.
///
/// ```asm
/// add %r1, %r2
/// ```
/// is equivalent to do the following mathematical operation:
/// $$ %r1 = %r1 + %r2 $$
struct Add;
impl Display for Add {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "ADD")
    }
}

impl Callable for Add {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let in1 = s.code.next().unwrap() as usize;
        let in2 = s.code.next().unwrap() as usize;

        s.reg.set(
            in1,
            s.reg.get::<u8>(in1) + s.reg.get::<u8>(in2)
        );

        None
    }

    fn len(&self) -> usize {
        2
    }
}

/// Substract two register and store the result in the first one.
///
/// ```asm
/// sub %r1, %r2
/// ```
/// is equivalent to do the following mathematical operation:
/// $$ %r1 = %r1 - %r2 $$
struct Sub;
impl Display for Sub {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "SUB")
    }
}

impl Callable for Sub {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let in1 = s.code.next().unwrap() as usize;
        let in2 = s.code.next().unwrap() as usize;

        s.reg.set(
            in1,
            s.reg.get::<u8>(in1) - s.reg.get::<u8>(in2)
        );

        None
    }

    fn len(&self) -> usize {
        2
    }
}

/// Multiply two register and store the result in the first one.
///
/// ```asm
/// mul %r1, %r2
/// ```
/// is equivalent to do the following mathematical operation:
/// $$ %r1 = %r1 * %r2 $$
struct Mul;
impl Display for Mul {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "MUL")
    }
}

impl Callable for Mul {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let in1 = s.code.next().unwrap() as usize;
        let in2 = s.code.next().unwrap() as usize;

        s.reg.set(
            in1,
            s.reg.get::<u8>(in1) * s.reg.get::<u8>(in2)
        );

        None
    }

    fn len(&self) -> usize {
        2
    }
}

/// Divide two register and store the result in the first one.
///
/// ```asm
/// div %r1, %r2
/// ```
/// is equivalent to do the following mathematical operation:
/// $$ %r1 = %r1 / %r2 $$
struct Div;
impl Display for Div {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "DIV")
    }
}

impl Callable for Div {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let in1 = s.code.next().unwrap() as usize;
        let in2 = s.code.next().unwrap() as usize;

        s.reg.set(
            in1,
            s.reg.get::<u8>(in1) / s.reg.get::<u8>(in2)
        );

        None
    }

    fn len(&self) -> usize {
        2
    }
}

/// Move data in the second register to the first register
///
/// ```asm
/// mov %r1, %r2
/// ```
/// is equivalent to do the following mathematical operation:
/// $$ %r1 = %r2 $$
struct Mov;
impl Display for Mov {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "MOV")
    }
}

impl Callable for Mov {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let in1 = s.code.next().unwrap() as usize;
        let in2 = s.code.next().unwrap() as usize;

        s.reg.set(
            in1,
            s.reg.get::<u8>(in2)
        );

        None
    }

    fn len(&self) -> usize {
        2
    }
}

/// Jump to the given address.
///
/// ```asm
/// jmp %r1
/// ```
struct Jmp;
impl Display for Jmp {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "JMP")
    }
}

impl Callable for Jmp {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let in1 = s.code.next().unwrap() as usize;

        s.code.jmp(
            s.reg.get::<u8>(in1) as usize
        );

        None
    }

    fn len(&self) -> usize {
        1
    }
}

/// Print the content of register 0.
struct Prt;
impl Display for Prt {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "PRT")
    }
}

impl Callable for Prt {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let data = s.reg.get::<u8>(0);

        match stdout().write(&[data]) {
            Ok(1) => None,
            Ok(o) => {
                eprintln!("Written {} of bytes instead of 1 for '{:?}'.", o, data);
                None
            }
            Err(e) => {
                eprintln!("Unable to print character '{:?}' : {}.", data, e);
                None
            }
        }
    }

    fn len(&self) -> usize {
        0
    }
}

/// Read a byte from stdin into register 0.
struct Rd;
impl Display for Rd {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "RD")
    }
}

impl Callable for Rd {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let rd: u8 = stdin()
            .bytes()
            .next()
            .unwrap_or(Ok(u8::max_value()))
            .unwrap_or(u8::max_value());
        s.reg.set::<u8>(0, rd);

        None
    }

    fn len(&self) -> usize {
        2
    }
}

/// Load the content of the given memory address to the given register.
///
/// ```asm
/// lda %reg, %mem
/// ```
struct Lda;
impl Display for Lda {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "LDA")
    }
}

impl Callable for Lda {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let reg = s.code.next().unwrap() as usize;
        let mem = s.code.next().unwrap() as usize;

        s.reg.set(
            reg,
            s.heap.get::<u8>(mem)
        );

        None
    }

    fn len(&self) -> usize {
        2
    }
}

/// Store the content of the given register into the given memory address.
///
/// ```asm
/// sta %reg, %mem
/// ```
struct Sta;
impl Display for Sta {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "STA")
    }
}

impl Callable for Sta {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let reg = s.code.next().unwrap() as usize;
        let mem = s.code.next().unwrap() as usize;

        s.heap.set(
            mem,
            s.reg.get::<u8>(reg)
        );

        None
    }

    fn len(&self) -> usize {
        2
    }
}

/// Store the given value to the given registry.
///
/// ```asm
/// cte %reg,#value
/// ```
struct Cte;
impl Display for Cte {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "CTE")
    }
}

impl Callable for Cte {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let reg = s.code.next().unwrap() as usize;
        let value = s.code.next().unwrap();

        s.reg.set(
            reg,
            value
        );

        None
    }

    fn len(&self) -> usize {
        2
    }
}

/// Read the file into a byte vector.
fn read_bin<P: AsRef<Path>>(path: P) -> Result<Vec<u8>, std::io::Error> {
    let mut file = File::open(path)?;
    let mut buf = Vec::new();
    file.read_to_end(&mut buf)?;

    Ok(buf)
}

pub fn main() -> Result<(), String> {
    let mut inst_array: Vec<Box<dyn Callable>> = vec![
        Box::new(Jmp),  // 0x0
        Box::new(Add),  // 0x1
        Box::new(Sub),  // 0x2
        Box::new(Mul),  // 0x3
        Box::new(Div),  // 0x4
        Box::new(Mov),  // 0x5
        Box::new(Prt),  // 0x6
        Box::new(Rd),   // 0x7
        Box::new(Lda),  // 0x8
        Box::new(Sta),  // 0x9
        Box::new(Cte),  // 0xa
        Box::new(Nop),  // 0xb
    ];

    let bc = read_bin(
        env::args()
            .nth(1)
            .map_or(
                Err("Missing file name."),
                |x| Ok(x)
            )?
    ).map_err(
            |e| format!("Error while reading the file: {:?}.", e)
    )?;

    let mut vm = Vm::construct()
        .code(&bc)
        .heap(255)
        .stack(255)
        .register(6)
        .verbose()
        .build(&mut inst_array);

    vm.interpret();

    Ok(())
}
