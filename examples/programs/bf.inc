%define r0 0
%define r1 1
%define r2 2
%define r3 3
%define r4 4
%define r5 5

%macro jmp 1
	db 0x00, %1
%endmacro

%macro add 2
	db 0x01, %1, %2
%endmacro

%macro sub 2
	db 0x02, %1, %2
%endmacro

%macro mul 2
	db 0x03, %1, %2
%endmacro

%macro div 2
	db 0x04, %1, %2
%endmacro

%macro mov 2
	db 0x05, %1, %2
%endmacro

%macro prt 0
	db 0x06
%endmacro

%macro rd 0
	db 0x07
%endmacro

%macro lda 2
	db 0x08, %1, %2
%endmacro

%macro sta 2
	db 0x09, %1, %2
%endmacro

%macro cte 2
	db 0x0a, %1, %2
%endmacro

%macro nop 0
	db 0x0b
%endmacro
