// Inspired from a Youtube video, see https://www.youtube.com/watch?v=DUNkdl0Jhgs
extern crate carabined as vmc;

use vmc::{Callable, State};

use std::fmt::Display;
use std::fs::File;
use std::io::{Read, Write};
use std::io::stdout;

type InstArr = Vec<Box<Callable>>;

// fn load_directories(path: &Path) -> InstArr {
    // let mut loaded: InstArr = Vec::new();

    // if path.is_dir() {
        // for dir in path.read_dir().expect("Weird problem encounter.") {
            // if let Ok(entry) = dir {
                // if entry.path().is_dir() {
                    // loaded.append(
                        // &mut load_directories(&entry.path())
                    // )
                // } else {
                    // if let Ok(plugin) = Plugin::new(entry.path()) {
                        // loaded.append(
                            // &mut plugin.register()
                        // );
                    // }
                // }
            // }
        // }
    // }

    // loaded
// }

fn read_inst(fname: &str) -> std::io::Result<Vec<u8>> {
    let mut file = File::open(fname)?;
    let mut inst: Vec<u8> = Vec::new();

    file.read_to_end(&mut inst)?;

    Ok(inst)
}

struct Nop;
impl Display for Nop {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "nop")
    }
}

impl Callable for Nop {
    fn call(&mut self, _s: &mut State) -> Option<usize> {
        None
    }

    fn len(&self) -> usize {
        0
    }
}

struct CPush;
impl Display for CPush {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "cpush")
    }
}

impl Callable for CPush {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let add = match s.code.next() {
            Some(x) => x,
            None => 0,
        };
        s.stack.push_single(add);

        None
    }

    fn len(&self) -> usize {
        1
    }
}

struct CEmit;
impl Display for CEmit {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "cemit")
    }
}

impl Callable for CEmit {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let c = match s.stack.pop_single() {
            Some(x) => x,
            None => 0,
        };
        let out = stdout();
        let mut handle = out.lock();
        match handle.write(&[c]) {
            Ok(_) => None,
            Err(err) => {
                println!("Error while writing to stdout: {}", err);
                None
            },
        }
    }

    fn len(&self) -> usize {
        1
    }
}

struct Halt;
impl Display for Halt {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "halt")
    }
}

impl Callable for Halt {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let end = s.code.len();

        s.code.jmp(end)
    }

    fn len(&self) -> usize {
        0
    }
}

pub fn main() {
    let fname = "examples/programs/hello.vm";
    let bc: Vec<u8> = match read_inst(fname) {
        Err(e) => panic!("Errors while reading the file {}: {}.", fname, e),
        Ok(v) => v,
    };
    // let inst_array: InstArr = Vec::new_with_capacity(256);
    let mut inst_array: InstArr = (0..256).map(|_x| Box::new(Nop) as Box<dyn Callable>).collect();
    inst_array['c' as usize] = Box::new(CPush);
    inst_array['e' as usize] = Box::new(CEmit);
    inst_array['h' as usize] = Box::new(Halt);

    let mut vm = vmc::Vm::construct()
        .code(&bc)
        .build(&mut inst_array);

    vm.interpret();
}
