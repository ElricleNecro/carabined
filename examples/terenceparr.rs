/// This is an example virtual machine build after the conference given by Terence Parr on "How to
/// build a Virtual Machine".
///
/// http://www.youtube.com/watch?v=OjaAToVkoTw
///
/// It implement the following instructions et:
/// | Instruction |     Arguments    | code | Description |
/// |-------------|:----------------:|:----:|-------------|
/// |   `iadd`    |                  | 0x1  | integer add (pop 2 operands, add, push result) |
/// |   `isub`    |                  | 0x2  | integer sub (pop 2 operands, sub, push result) |
/// |   `imul`    |                  | 0x3  | integer mul (pop 2 operands, mul, push result) |
/// |   `ilt`     |                  | 0xa  | integer less than |
/// |   `ieq`     |                  | 0xb  | integer equal |
/// |   `br`      | `addr`           | 0x14 | branch to address |
/// |   `brt`     | `addr`           | 0x15 | branch to address if true |
/// |   `brf`     | `addr`           | 0x16 | branch to address if false |
/// |   `iconst`  | `value`          | 0x1e | push integer constant |
/// |   `load`    | `addr`           |      | load local variable |
/// |   `gload`   | `addr`           |      | load global variable |
/// |   `store`   | `addr`           |      | store local variable |
/// |   `gstore`  | `addr`           |      | store global variable |
/// |   `print`   |                  | 0x28 | print (pop 1 operand, print it) |
/// |   `pop`     |                  | 0x29 | toss out the top of the stack |
/// |   `call `   | `addr, num_args` | 0x30 | call the routine at adress `addr` with `num_args` |
/// |   `ret`     |                  | 0x31 | Return from function, expected result on top of stack |
/// |   `halt`    |                  | 0xff | Stop the virtual machine |

extern crate bincode;
extern crate carabined;

use bincode::serialize;

use carabined::{Callable, State, Vm};

use std::fmt::{Display, Formatter, self};
use std::io::{stdout, Write};

struct Nop;
impl Display for Nop {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "NOP")
    }
}
impl Callable for Nop {
    fn call(&mut self, _s: &mut State) -> Option<usize> {
        None
    }

    fn len(&self) -> usize {
        0
    }
}

struct IAdd;
impl Display for IAdd {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "IADD")
    }
}
impl Callable for IAdd {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let right: u32 = s.stack.pop().unwrap();
        let left:  u32  = s.stack.pop().unwrap();

        s.stack.push(left + right);

        None
    }

    fn len(&self) -> usize {
        0
    }
}

struct ISub;
impl Display for ISub {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "ISUB")
    }
}
impl Callable for ISub {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let right: u32 = s.stack.pop().unwrap();
        let left:  u32  = s.stack.pop().unwrap();

        s.stack.push(left + right);

        None
    }

    fn len(&self) -> usize {
        0
    }
}

struct IMul;
impl Display for IMul {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "IMUL")
    }
}
impl Callable for IMul {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let right: u32 = s.stack.pop().unwrap();
        let left:  u32  = s.stack.pop().unwrap();

        s.stack.push(left + right);

        None
    }

    fn len(&self) -> usize {
        0
    }
}

struct IConst;
impl Display for IConst {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "ICONST")
    }
}
impl Callable for IConst {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let value: u32 = s.code.next().unwrap() as u32;

        s.stack.push(value);

        None
    }

    fn len(&self) -> usize {
        1
    }
}

struct Ilt;
impl Display for Ilt {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "ILT")
    }
}
impl Callable for Ilt {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let right: u32 = s.stack.pop().unwrap();
        let left:  u32  = s.stack.pop().unwrap();

        s.stack.push(right < left);

        None
    }

    fn len(&self) -> usize {
        0
    }
}

struct Ieq;
impl Display for Ieq {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "IEQ")
    }
}
impl Callable for Ieq {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let right: u32 = s.stack.pop().unwrap();
        let left:  u32  = s.stack.pop().unwrap();

        s.stack.push(right == left);

        None
    }

    fn len(&self) -> usize {
        0
    }
}

struct Br;
impl Display for Br {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "BR")
    }
}
impl Callable for Br {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let addr = s.code.next().unwrap() as usize;

        s.code.jmp(addr);

        None
    }

    fn len(&self) -> usize {
        1
    }
}

struct Brt;
impl Display for Brt {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "BRF")
    }
}
impl Callable for Brt {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let addr = s.code.next().unwrap() as usize;
        let value: bool = s.stack.pop().unwrap();

        if value {
            s.code.jmp(addr);
        }

        None
    }

    fn len(&self) -> usize {
        1
    }
}

struct Brf;
impl Display for Brf {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "BRF")
    }
}
impl Callable for Brf {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let addr = s.code.next().unwrap() as usize;
        let value: bool = s.stack.pop().unwrap();

        if ! value {
            s.code.jmp(addr);
        }

        None
    }

    fn len(&self) -> usize {
        1
    }
}

struct Halt;
impl Display for Halt {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "HALT")
    }
}
impl Callable for Halt {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let end = s.code.len();

        s.code.jmp(end)
    }

    fn len(&self) -> usize {
        0
    }
}

struct Print;
impl Display for Print {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "PRINT")
    }
}
impl Callable for Print {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let value = s.stack.pop::<u32>().unwrap();

        let out = stdout();
        let mut handle = out.lock();
        match handle.write(&serialize(&value).unwrap()[..]) {
            Ok(_) => None,
            Err(err) => {
                println!("Error while writing to stdout: {}", err);
                None
            },
        }
    }

    fn len(&self) -> usize {
        0
    }
}

struct Pop;
impl Display for Pop {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "POP")
    }
}
impl Callable for Pop {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let _ = s.stack.pop::<u32>();

        None
    }

    fn len(&self) -> usize {
        0
    }
}

struct GLoad;
impl Display for GLoad {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "GLOAD")
    }
}
impl Callable for GLoad {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let addr = s.code.next().unwrap() as usize;

        s.stack.push(
            s.heap.get::<u32>(addr)
        );

        None
    }

    fn len(&self) -> usize {
        1
    }
}

struct GStore;
impl Display for GStore {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "GSTORE")
    }
}
impl Callable for GStore {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        let addr = s.code.next().unwrap() as usize;
        let value: u32 = s.stack.pop().unwrap();

        s.heap.set(addr, value);

        None
    }

    fn len(&self) -> usize {
        1
    }
}

struct Call;
impl Display for Call {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "CALL")
    }
}
impl Callable for Call {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        // Address of the function:
        let addr = s.code.next().unwrap();
        // Number of arguments of the function:
        let num_args = s.code.next().unwrap();

        // Saving the location of the 'next' step:
        s.stack.push(s.code.eip);
        // Saving the stack pointer:
        s.fp = s.stack.sp();
        // Jumping to the function:
        s.code.jmp(addr as usize);

        None
    }

    fn len(&self) -> usize {
        2
    }
}

struct Ret;
impl Display for Ret {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "RET")
    }
}
impl Callable for Ret {
    fn call(&mut self, s: &mut State) -> Option<usize> {
        // Extracting the return value:
        let ret_value = s.stack.pop::<u32>().unwrap();

        // Restoring the stack:
        s.stack.remove_to(s.fp);

        // Jumping right after the call:
        s.code.jmp(s.stack.pop_single().unwrap() as usize);

        // Putting back the return value on the stack:
        s.stack.push(ret_value);

        None
    }

    fn len(&self) -> usize {
        0
    }
}

pub fn main() {
    let bc = vec![
        0x1e, 1,    // 0
        0x1e, 2,    // 1
        0x1,        // 2
        0x1e, 3,    // 3
        0xb,        // 4
        0x16, 10,   // 5
        0x1e, 120,  // 6
        0x28,       // 7
        0x1e, 10,   // 8
        0x28,       // 9
        0xff        // 10
    ];
    let mut inst_array: Vec<Box<dyn Callable>> = (0..256).map(|_x| Box::new(Nop) as Box<dyn Callable>).collect();
    inst_array[0x1 ] = Box::new(IAdd);
    inst_array[0x2 ] = Box::new(ISub);
    inst_array[0x3 ] = Box::new(IMul);
    inst_array[0xa ] = Box::new(Ilt);
    inst_array[0xb ] = Box::new(Ieq);
    inst_array[0x14] = Box::new(Br);
    inst_array[0x15] = Box::new(Brt);
    inst_array[0x16] = Box::new(Brf);
    inst_array[0x1e] = Box::new(IConst);
    inst_array[0x28] = Box::new(Print);
    inst_array[0x29] = Box::new(Pop);
    inst_array[0x30] = Box::new(Call);
    inst_array[0x31] = Box::new(Ret);
    inst_array[0xff] = Box::new(Halt);

    let mut vm = Vm::construct()
        .code(&bc)
        .verbose()
        .build(&mut inst_array);

    vm.interpret();
}
